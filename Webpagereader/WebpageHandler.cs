﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Webpagereader
{
   public class WebpageHandler
    {
        string result = String.Empty;
        public async Task<string> readWebPage(string url)
        {
           
            try
            {
              result = await new WebClient().DownloadStringTaskAsync(address: url);
            } catch(Exception e)
            {
                
            }
            return result;
        }

        public async Task<string> downloadScript(string url)
        {
            result = String.Empty;
            try
            {
                result = await new WebClient().DownloadStringTaskAsync(address: url);
            } catch(Exception e)
            {

            }
            return result;
        }
    }
}
