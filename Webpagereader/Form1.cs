﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Webpagereader
{
    public partial class Form1 : Form
    {
        WebpageHandler webpageHandler;
        string result;
        List<string> urls;
        List<string> Articles = new List<string>();
        Trimmer trimmer = new Trimmer();
        public Form1()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Method headline uses the trimmer class to encode to UTF8 and then get teh free article link to the output textbox.
        /// </summary>
        /// <param name="sender">A sender object.</param>
        /// <param name="e">An EventArgs.</param>
        private async void GetHeadlineButton_Click(object sender, EventArgs e)
        {
            webpageHandler = new WebpageHandler();
            ArticlesCheckedCombobox.Items.Clear();
            urls = new List<string>();
            urls = UrlTextbox.Text.Split(',').ToList();
            List<string> individualArticleList = new List<string>();
            //url.Add("http://www.allehanda.se/alla");
            //url.Add("http://www.allehanda.se/sport");
            foreach (string u in urls)
            {
                Task<string> resultTaskresult = webpageHandler.readWebPage(u);
                result = await resultTaskresult;
                result = trimmer.defaultEncodingToUTF8(result);
                //List<string> ArticleResult;
                Articles.AddRange(trimmer.FindArticleLinks(result, "Provläs"));
               
            }
            Articles = Articles.Distinct().ToList();
            foreach(string article in Articles)
            {
                ArticlesCheckedCombobox.Items.Add(article);
            }
        }
        /// <summary>
        /// PreviewButton_Click Handles the event when the Preview button is pressed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PreviewButton_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < ArticlesCheckedCombobox.Items.Count; i++)
            {
                if (ArticlesCheckedCombobox.GetItemChecked(i))
                {
                    if (trimmer.validArticle(ArticlesCheckedCombobox.Items[i]))
                    {
                        PreviewForm PreviewForm = new PreviewForm(ArticlesCheckedCombobox.Items[i].ToString());
                        PreviewForm.ShowDialog();
                    }
                }
            }
        }

        /// <summary>
        /// Method Downloadbutton_Click handles the event when the download button is pressed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void Downloadbutton_Click(object sender, EventArgs e)
        {
            WebpageHandler webpageHandler = new WebpageHandler();
            FH filehandler = new FH();
            List<string> scriptLinks = new List<string>();
            for (int i = 0; i < ArticlesCheckedCombobox.Items.Count; i++)
            {
                if(ArticlesCheckedCombobox.GetItemChecked(i))
                {
                    if (trimmer.validArticle(ArticlesCheckedCombobox.Items[i]))
                    {
                        Task<string> resultTaskresult = webpageHandler.readWebPage(ArticlesCheckedCombobox.Items[i].ToString());
                        result = await resultTaskresult;
                        result = trimmer.defaultEncodingToUTF8(result);
                        string pattern = @"<script *src\s*=\s*['""]([\w/\.\d\s-]*)[""']>|<link[/\s\w=""\d]*href=['""]([\.\d\w\\/-]*)['""][\s\w=""'/]*>";
                        scriptLinks = trimmer.FindLinks(result, pattern);
                        filehandler.WriteToFile(result);
                    }
                }
            }
        }
    }
}
