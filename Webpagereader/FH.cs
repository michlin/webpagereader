﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Webpagereader
{
    public class FH
    {
        /// <summary>
        /// Method WriteToFile opens up dialog window to let the user save the htm file of choice (article). 
        /// </summary>
        /// <param name="text"></param>
        public void WriteToFile(string text)
        {
            SaveFileDialog SaveFileDialog = new SaveFileDialog();
            SaveFileDialog.Filter = "htm files (*.htm)|";
            SaveFileDialog.FilterIndex = 1;
            SaveFileDialog.RestoreDirectory = true;
            if (SaveFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    File.WriteAllText(SaveFileDialog.FileName, text);
                }
                catch(Exception e)
                {
                    MessageBox.Show("Error with filewriting, try again", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
