﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Webpagereader
{
    public partial class PreviewForm : Form
    {
        String Url = String.Empty;
        public PreviewForm()
        {
            InitializeComponent();
        }
        public PreviewForm(string webpage)
        {
            InitializeComponent();
            NavigateToPage();
            Url = webpage;
            
        }

        private void NavigateToPage()
        {
            PreviewWebbrowser.Navigate(Url);
        }
    }
}
