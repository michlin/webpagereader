﻿namespace Webpagereader
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.GetHeadLineButton = new System.Windows.Forms.Button();
            this.DownloadButton = new System.Windows.Forms.Button();
            this.UrlTextbox = new System.Windows.Forms.TextBox();
            this.UrlsTextboxLabel = new System.Windows.Forms.Label();
            this.Outputlabel = new System.Windows.Forms.Label();
            this.PreviewButton = new System.Windows.Forms.Button();
            this.ArticlesCheckedCombobox = new System.Windows.Forms.CheckedListBox();
            this.SuspendLayout();
            // 
            // GetHeadLineButton
            // 
            this.GetHeadLineButton.Location = new System.Drawing.Point(12, 279);
            this.GetHeadLineButton.Name = "GetHeadLineButton";
            this.GetHeadLineButton.Size = new System.Drawing.Size(75, 23);
            this.GetHeadLineButton.TabIndex = 0;
            this.GetHeadLineButton.Text = "Get headline";
            this.GetHeadLineButton.UseVisualStyleBackColor = true;
            this.GetHeadLineButton.Click += new System.EventHandler(this.GetHeadlineButton_Click);
            // 
            // DownloadButton
            // 
            this.DownloadButton.Location = new System.Drawing.Point(93, 279);
            this.DownloadButton.Name = "DownloadButton";
            this.DownloadButton.Size = new System.Drawing.Size(99, 23);
            this.DownloadButton.TabIndex = 2;
            this.DownloadButton.Text = "Download article";
            this.DownloadButton.UseVisualStyleBackColor = true;
            this.DownloadButton.Click += new System.EventHandler(this.Downloadbutton_Click);
            // 
            // UrlTextbox
            // 
            this.UrlTextbox.Location = new System.Drawing.Point(13, 39);
            this.UrlTextbox.Name = "UrlTextbox";
            this.UrlTextbox.Size = new System.Drawing.Size(630, 20);
            this.UrlTextbox.TabIndex = 3;
            this.UrlTextbox.Text = "Please enter addresses splited by either comma or space";
            // 
            // UrlsTextboxLabel
            // 
            this.UrlsTextboxLabel.AutoSize = true;
            this.UrlsTextboxLabel.Location = new System.Drawing.Point(13, 20);
            this.UrlsTextboxLabel.Name = "UrlsTextboxLabel";
            this.UrlsTextboxLabel.Size = new System.Drawing.Size(34, 13);
            this.UrlsTextboxLabel.TabIndex = 4;
            this.UrlsTextboxLabel.Text = "URLs";
            // 
            // Outputlabel
            // 
            this.Outputlabel.AutoSize = true;
            this.Outputlabel.Location = new System.Drawing.Point(13, 62);
            this.Outputlabel.Name = "Outputlabel";
            this.Outputlabel.Size = new System.Drawing.Size(42, 13);
            this.Outputlabel.TabIndex = 5;
            this.Outputlabel.Text = "Output:";
            // 
            // PreviewButton
            // 
            this.PreviewButton.Location = new System.Drawing.Point(199, 279);
            this.PreviewButton.Name = "PreviewButton";
            this.PreviewButton.Size = new System.Drawing.Size(75, 23);
            this.PreviewButton.TabIndex = 6;
            this.PreviewButton.Text = "Preview";
            this.PreviewButton.UseVisualStyleBackColor = true;
            this.PreviewButton.Click += new System.EventHandler(this.PreviewButton_Click);
            // 
            // ArticlesCheckedCombobox
            // 
            this.ArticlesCheckedCombobox.FormattingEnabled = true;
            this.ArticlesCheckedCombobox.Location = new System.Drawing.Point(16, 79);
            this.ArticlesCheckedCombobox.Name = "ArticlesCheckedCombobox";
            this.ArticlesCheckedCombobox.Size = new System.Drawing.Size(627, 184);
            this.ArticlesCheckedCombobox.TabIndex = 7;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(656, 314);
            this.Controls.Add(this.ArticlesCheckedCombobox);
            this.Controls.Add(this.PreviewButton);
            this.Controls.Add(this.Outputlabel);
            this.Controls.Add(this.UrlsTextboxLabel);
            this.Controls.Add(this.UrlTextbox);
            this.Controls.Add(this.DownloadButton);
            this.Controls.Add(this.GetHeadLineButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button GetHeadLineButton;
        private System.Windows.Forms.Button DownloadButton;
        private System.Windows.Forms.TextBox UrlTextbox;
        private System.Windows.Forms.Label UrlsTextboxLabel;
        private System.Windows.Forms.Label Outputlabel;
        private System.Windows.Forms.Button PreviewButton;
        private System.Windows.Forms.CheckedListBox ArticlesCheckedCombobox;
    }
}

