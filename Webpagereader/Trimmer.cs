﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Webpagereader
{
    /// <summary>
    /// Class Trimmer handles the trimming of the hyperlink to the free article.
    /// </summary>
    public class Trimmer
    {
        string Result,TempResult = String.Empty;
        List<string> ArticleLinks = new List<string>();
        /// <summary>
        /// Method defaultEncodingToUTF8 handles encoding from the default encoding to the UTF-8 format.
        /// </summary>
        /// <param name="text">The text that is going to be encoded to UTF8.</param>
        /// <returns>encoded UTF8 string.</returns>
        public string defaultEncodingToUTF8(string text)
        {
            try
            {
                byte[] bytes = Encoding.Default.GetBytes(text);
                Result = Encoding.UTF8.GetString(bytes);
            }
            catch(Exception e)
            {
                Result = e.StackTrace;
            }
            return Result;
        }
        /// <summary>
        /// validArticle is a second check to find out if an article is valid.
        /// </summary>
        /// <param name="itemChecked"></param>
        /// <returns></returns>
        public Boolean validArticle(Object itemChecked)
        {
            Boolean result = true;
            if (itemChecked.ToString().Contains("No free articles was found"))
                result = false;

            return result;
        }
        /// <summary>
        /// Method FindArticleLinks finds the article within the provided link.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="word"></param>
        /// <returns></returns>      
        public List<string> FindArticleLinks(string text,string word)
        {
            int i = 0;
            try
            {
                foreach (Match match in Regex.Matches(text, word))
                {
                    TempResult = text.Substring(text.IndexOf(word)+i, 500);
                    if(TempResult.Contains("<a href="))
                    {
                        TempResult = TempResult.Substring(TempResult.IndexOf("<a href=") + 10);
                        TempResult = TempResult.Substring(0, TempResult.IndexOf(" "));
                        TempResult = TempResult.Replace("\"", "");
                        TempResult = TempResult.Insert(0, "https://www.allehanda.se/");
                        ArticleLinks.Add(TempResult);
                    }
                    i++;
               }
            }
            catch(Exception e)
            {
                TempResult = e.StackTrace;
            }
            ArticleLinks = ArticleLinks.Distinct().ToList();
            return ArticleLinks;
        }
        /// <summary>
        /// Method FindLinks finds script links within a string text given a string pattern.
        /// </summary>
        /// <param name="text">A given text.</param>
        /// <param name="pattern">A given Regex pattern. Can be words aswell.</param>
        /// <returns></returns>
        public List<string> FindLinks(string text, string pattern)
        {
            List<string> ListResult = new List<string>();
            foreach (Match match in Regex.Matches(text,pattern))
            {
                if(match.ToString().Contains("href=") && !(match.ToString().Contains("manifest")))
                {
                    TempResult = match.ToString().Substring(match.ToString().IndexOf("href=") + 6);
                    TempResult = TempResult.Substring(0, TempResult.IndexOf(" "));
                    TempResult = TempResult.Replace("\"", "");
                    TempResult = TempResult.Insert(0, "https://www.allehanda.se");
                    ListResult.Add(TempResult);
                }
                
            }
            return ListResult;
        }
    }
}
